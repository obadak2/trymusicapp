package com.example.trymusicplayer;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

//import androidx.media2.exoplayer.external.SimpleExoPlayer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.example.trymusicplayer.services.MyService;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.ext.ima.ImaAdsLoader;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.source.ads.AdsMediaSource;
import com.google.android.exoplayer2.ui.PlayerNotificationManager;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

public class ExoPlayer extends AppCompatActivity {
    Button btnPlay, btnNext, btnPrevious, btnFastForward, btnFastBackWard;
    TextView txtSongName;
    PlayerView playerView;
    SimpleExoPlayer player;
    String musicURL;
    ImaAdsLoader adsLoader;
    ImageView imageView;
    String songName = " ";
    Bundle bundle;
    String buttonStatus;
    String message;
    PlayerNotificationManager playerNotificationManager;
    public static final Uri AD_TAG_URI = Uri.parse("https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/ad_rule_samples&ciu_szs=300x250&ad_rule=1&impl=s&gdfp_req=1&env=vp&output=vmap&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ar%3Dpremidpostlongpod&cmsid=496&vid=short_tencue&correlator=");
    Intent intent1;
//    private ImaAdsLoader adsLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.music_wor);
        Intent intent = getIntent();
        bundle = intent.getExtras();
        songName = bundle.getString("songname");
        musicURL = bundle.getString("firstURL");
        System.out.println(songName);
        System.out.println(musicURL);
        adsLoader = new ImaAdsLoader(this, AD_TAG_URI);
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter("intentKey"));

    }



    @SuppressLint("RestrictedApi")
    @Override
    protected void onStart() {
        super.onStart();
        //Assigning the address of the android Materials
        btnPlay = (Button) findViewById(R.id.BtnPlay);
        btnNext = (Button) findViewById(R.id.BtnNext);
        btnPrevious = (Button) findViewById(R.id.BtnPrevious);
        btnFastForward = (Button) findViewById(R.id.BtnFastForward);
        btnFastBackWard = (Button) findViewById(R.id.BtnFastRewind);
        txtSongName = (TextView) findViewById(R.id.SongTxt);
        imageView = (ImageView) findViewById(R.id.MusicImage);

        txtSongName.setSelected(true);
        txtSongName.setText(songName);

        intent1 = new Intent(this, com.example.trymusicplayer.services.MyService.class);
        buttonStatus = "play";
        intent1.putExtra("BuStatus",buttonStatus);
        intent1.putExtra("songname",songName);
//        this.startService(intent1);
        Util.startForegroundService(getApplicationContext(), intent1);
        //Implementing OnClickListener for Play and Pause Button
        btnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (message.contains("play")) {
                    //Checking playing any songs or not
                    //setting the play icon
                    btnPlay.setBackgroundResource(R.drawable.play_song_icon);
                    //Pausing the current media
                    buttonStatus = "pause";
                    intent1.putExtra("BuStatus", buttonStatus);
                } else {
                    buttonStatus = "play";
                    intent1.putExtra("BuStatus", buttonStatus);
                    //Setting the pause icon
                    btnPlay.setBackgroundResource(R.drawable.pause_song_icon);
                    //Starting the media player
                }
                Util.startForegroundService(getApplicationContext(), intent1);
//                startService(intent1);
                //Creating the Animation
                TranslateAnimation moveAnim = new TranslateAnimation(-25, 25, -25, 25);
                moveAnim.setInterpolator(new AccelerateInterpolator());
                moveAnim.setDuration(600);
                moveAnim.setFillEnabled(true);
                moveAnim.setFillAfter(true);
                moveAnim.setRepeatMode(Animation.REVERSE);
                moveAnim.setRepeatCount(1);

                //Setting the Animation for the Image
                imageView.startAnimation(moveAnim);
            }
        });
        btnFastForward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonStatus = "fastF";
                intent1.putExtra("BuStatus", buttonStatus);
                Util.startForegroundService(getApplicationContext(), intent1);
//                startService(intent1);
            }
        });
        btnFastBackWard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonStatus = "fastB";
                intent1.putExtra("BuStatus", buttonStatus);
                Util.startForegroundService(getApplicationContext(), intent1);
//                startService(intent1);
            }
        });

    }


    @SuppressLint("RestrictedApi")
    @Override
    protected void onStop() {
        super.onStop();
    }
    @Override
    protected void onDestroy() {
//        adsLoader.release();

        super.onDestroy();
    }

    //Used to catch a message from service
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            message = intent.getStringExtra("key");
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        }
    };
}