package com.example.trymusicplayer.services;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.media2.exoplayer.external.upstream.BandwidthMeter;

import com.example.trymusicplayer.R;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.RenderersFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerNotificationManager;

import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import java.io.File;
import java.util.ArrayList;

import static com.google.android.exoplayer2.ui.PlayerNotificationManager.*;

public class MyService extends Service implements ExoPlayer.EventListener {
    //    MediaPlayer player;
    SimpleExoPlayer player;
    ArrayList<File> mySongs;
    String musicURL;
    int position;
    String songName;
    Intent intent1;
    Bundle bundle;
    PlayerNotificationManager playerNotificationManager;
    public static final String CHANNEL_ID = "player_channel";
    public static final int NOTIFICATION_ID = 2;
    private Handler mainHandler;
    private RenderersFactory renderersFactory;
    private BandwidthMeter bandwidthMeter;
    private LoadControl loadControl;
    private ExtractorsFactory extractorsFactory;
    private TrackSelection.Factory trackSelectionFactory;
    private TrackSelector trackSelector;
    private PlayerView playerView;
    private String message;
    private DataSource.Factory dataSourceFactory;
    private ProgressiveMediaSource mediaSource;



    @Override
    @SuppressLint("RestrictedApi")
    public void onCreate() {
        player=new SimpleExoPlayer.Builder(this).build();
        musicURL = getString(R.string.firstUrl);
        dataSourceFactory = new DefaultDataSourceFactory(this, Util.getUserAgent(this, getString(R.string.app_name)));
        mediaSource = new ProgressiveMediaSource.Factory(dataSourceFactory).createMediaSource(Uri.parse(musicURL));
        player.prepare(mediaSource);
        player.setPlayWhenReady(true);
        sendMessageToActivity("play");
//        Log.d("oncreate",message);
    }


    //It is used for inter process communication(IPC).
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        this.message = intent.getExtras().getString("BuStatus");
        this.songName = intent.getExtras().getString("songname");
        Log.d("onstart",message);

//        songName = musicURL.substring(musicURL.lastIndexOf('/') + 1, musicURL.length());
        playerNotificationManager = playerNotificationManager.createWithNotificationChannel(
                this,CHANNEL_ID,R.string.channel_name,NOTIFICATION_ID,
                new MediaDescriptionAdapter(){

                    @Override
                    public CharSequence getCurrentContentTitle(Player player) {
                        return songName;
                    }

                    @Nullable
                    @Override
                    public PendingIntent createCurrentContentIntent(Player player) {
                        return null;
                    }

                    @Nullable
                    @Override
                    public CharSequence getCurrentContentText(Player player) {
                        return "text";
                    }

                    @Nullable
                    @Override
                    public Bitmap getCurrentLargeIcon(Player player, PlayerNotificationManager.BitmapCallback callback) {
                        return null;
                    }
                }
        );

        playerNotificationManager.setNotificationListener(new NotificationListener() {
            @Override
            public void onNotificationPosted(int notificationId, Notification notification, boolean ongoing) {
                startForeground(notificationId, notification);
            }
            @Override
            public void onNotificationCancelled(int notificationId, boolean dismissedByUser) {
                stopSelf();
            }
        });

        playerNotificationManager.setPlayer(player);
        exoPlayer(message);
        return START_STICKY;
    }


    //Send a message to an activity
    private void sendMessageToActivity(String msg) {
        Intent intent = new Intent("intentKey");
        // You can also include some extra data.
        intent.putExtra("key", msg);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
    }


    //Runs the song and control it
    public void exoPlayer(String msg){
        if (msg.contains("play")){
            sendMessageToActivity("play");
            player.setPlayWhenReady(true);
        }
        else if(msg.contains("pause")){
            sendMessageToActivity("pause");
            player.setPlayWhenReady(false);
            player.getPlaybackState();
        }
        else {
            if (msg.contains("fastF")){
                int pl = player.getPlaybackState();
                System.out.println("player state");
                System.out.println(pl);
                player.setPlayWhenReady(false);
                player.seekTo(player.getCurrentPosition()+1000);
                player.setPlayWhenReady(true);
                System.out.println("current pos");
                System.out.println(player.getCurrentPosition());

//                player.setPlayWhenReady(true);

            }
            else if (msg.contains("fastB")){
                player.getPlaybackState();
                player.seekTo(player.getCurrentPosition()-1000);
//            player.setPlayWhenReady(true);
            }
            player.setPlayWhenReady(true);
        }
    }


    //Used to kill the service when clear the open apps
    @Override
    public void onTaskRemoved(Intent rootIntent) {
        System.out.println("onTaskRemoved called");
        super.onTaskRemoved(rootIntent);
        //do something you want
        //stop service
        this.stopSelf();
    }


    @Override
    public void onDestroy() {
        playerNotificationManager.setPlayer(null);
        player.release();
        player=null;
        super.onDestroy();
    }
}
